import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { useHistory } from 'react-router-dom'

const usernameConfig = {
  required: true,
  minLength: 2,
};

const LoginForm = () => {
  const {register, handleSubmit, formState: { errors },} = useForm();

  //local state
  const [ loading, setLoading] = useState(false)  
  const [ apiError, setApiError] = useState(null)

  // side effects
  useEffect(() => {
    
  }, []) 

  // event handler
  const onSubmit = async ({ username }) => {
    setLoading(true)
    const [error, user] = await loginUser(username)
    if (error !== null) {
      setApiError(error)
    }
    if (user !== null) {
      storageSave('appUsers', user)
    }
    setLoading(false)
    
  };

  // render functions
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === "required") {
      return <span>Username is required</span>;
    }
    if (errors.username.type === "minLength") {
      return <span>Username is too short!</span>;
    }
  })();

  return (
    <>
      <h2>Whats your name</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <label htmlFor="username"></label>
          <input
            type="text"
            placeholder="Username"
            {...register("username", usernameConfig)}
          />
          
            
            <button type="submit" disabled={loading}>Continue</button>
            { loading && <p>Loggin in...</p>}
           {errorMessage}
           {apiError && <p>{apiError}</p>}
          
        </fieldset>
      </form>
    </>
  );
};

export default LoginForm;
