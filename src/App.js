
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
  NavLink
} from 'react-router-dom'
import Login from './views/Login';
import Translations from './views/Translations'
import Profile from './views/Profile'
import { useContext, useEffect } from 'react';
import { ProfileContext } from './hoc/ProfileContext';



function App() {

  

  return ( 
    <BrowserRouter>
    <div className="App">
      <Routes>
        <Route path="/" element={ <Login/>} /> 
        <Route path='/Translations' element={<Translations/>}  />
        <Route path="/Profile" element={< Profile/>} />
      </Routes>
      <nav>
        <NavLink to="/Profile">Profile</NavLink>
      </nav>
      {/* <SimpleForm></SimpleForm> */}
    </div>
    </BrowserRouter>
  );
}

export default App;
