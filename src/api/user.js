import { createHeaders } from './index'
const apiURL = process.env.REACT_APP_API_URL

// checkForUser checks if a specific username is registered on the used api and throws an error if the response 
//from the api is not ok. If there is no error, then checkForUser will return either an null value for error and 
//a user, or it will catch and return an error message and an empty object.
const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiURL}?username=${username}`)
        if (!response.ok) {
            throw new Error('Could not complete request.')
        }
        const data = await response.json()
        return [null, data]
    } catch (error) {
        return[error.message, []]
    }
}

// createUser tries to create a response with properties where the username becomes the unique check in api for later
//login. The body also has a property of an empty array where the history if that specific users translations will be
//stored on the api.
const createUser = async (username) => {
    try {
        const response = await fetch(apiURL, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error('Could not create new user with username. ' + username)
        }
        const data = await  response.json()
        return [null, data]
    } catch (error) {
        return[error.message, []]
    }
}

export const loginUser = async (username) =>{
    const [checkError, user] = await checkForUser(username)
   
   if (checkError !== null) {
        return [checkError, null]
   }
//    user.pop() helps returning the object instead of the array where the object is located.
    if (user.length > 0) {
        return [ null, user.pop()]
    }

    return await createUser(username)  
}