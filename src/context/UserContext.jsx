import { Children, createContext, useContext, useState } from "react";

// context object -> exposing the state
const UserContext = createContext()


// returns an object {user and setUser}
export const useUser = () => {
    return useContext(UserContext) 
}
// provider -> managing state 
const UserProvider = ({Children}) => {
    
    const [ user, setUser ] = useState(null)

    const state = {
        user,
        setUser
    }

    return (
        <UserContext.Provider value={state}>
            {Children}

        </UserContext.Provider>
     )
}
export default UserProvider