import UserProvider from "./UserContext"

const AppContext = ({Children}) => {
    
    return (
        <UserProvider>
            {Children}
        </UserProvider>
    )
}
 export default AppContext