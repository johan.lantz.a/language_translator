# Language Translator

Sign language translator. React web application.

## Install

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Contributors
##### [Johan Lantz (@johan.lantz.a)](@johan.lantz.a)
##### [Pontus Gillson @d0senb0den](@d0senb0den)
